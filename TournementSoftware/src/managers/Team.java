package managers;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Team {
	private int number = 0;
	private int rank = 0;
	private String name;
	private ArrayList<Integer> scores = new ArrayList<Integer>();
	private ArrayList<Integer> asteroids = new ArrayList<Integer>();
	private ArrayList<Integer> spaceDust = new ArrayList<>();
	private ArrayList<Integer> climb = new ArrayList<>();
	private ArrayList<Integer> fouls = new ArrayList<Integer>();
	private ArrayList<Integer> coop = new ArrayList<>();

	public Team(int number, String name) {
		this.number = number;
		this.name = name;
	}

	//(Total, Flags, Home, Fouls)
	public void addScore(int[] score) {
		System.out.println("Team " + number + " scores: " + Arrays.toString(score));
		scores.add(score[0]);
		asteroids.add(score[1]);
		spaceDust.add(score[2]);
		climb.add(score[3]);
		fouls.add(score[4]);
		coop.add(score[5]);
	}

	public int getNumber() {
		return number;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public float getAverageScore() {
		if(scores.size() == 0){
			return 0;
		}
		float sum = 0;
		for (int score : scores) {
			sum += score;
		}
		return sum / scores.size();
	}

	public float getAverageAsteroids() {
		if(asteroids.size() == 0){
			return 0;
		}
		float sum = 0;
		for (int flag : asteroids) {
			sum += flag;
		}
		return sum / asteroids.size();
	}

	public float getAverageSpaceDust() {
		if(spaceDust.size() == 0){
			return 0;
		}
		float sum = 0;
		for (int home : spaceDust) {
			sum += home;
		}
		return sum / spaceDust.size();
	}

	public float getAverageClimb() {
		if(climb.size() == 0) return 0;

		float sum = 0;
		for(int points : climb) {
			sum += points;
		}

		return sum / climb.size();
	}
	
	public float getAverageFouls() {
		if(fouls.size() == 0){
			return 0;
		}
		float sum = 0;
		for (int foul : fouls) {
			sum += foul;
		}
		return sum / fouls.size();
	}

	public float getAverageCoop() {
		if(coop.isEmpty()) {
			return 0;
		}

		float sum = 0;

		for (int points : coop) {
			sum += points;
		}

		return sum / coop.size();
	}

	public String getName() {
		return name;
	}

	public int getMaxScore() {
		int max = 0;
		for (int score : scores) {
			max = Math.max(max, score);
		}
		return max;
	}

}
