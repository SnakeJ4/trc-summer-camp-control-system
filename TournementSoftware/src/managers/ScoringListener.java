package managers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ScoringListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {

		int position = e.getModifiers() >> 8;
		int count = (byte)(e.getModifiers() & 0b11111111);

		ScoringManager.getInstance().setCounts(position, count);
		Tournement.getWebServer().reformatWebpage();
	}

}
